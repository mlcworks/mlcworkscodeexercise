<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

require_once realpath(__DIR__.'/vendor/autoload.php');
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'local' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '3UMD1I5Q9T/UBybAtQ31arSFi2MBt65DVZralxScudTBgeibgcAzgSz6PJetR59cLJi0PP4JEYcKRGCg9CcU6g==');
define('SECURE_AUTH_KEY',  '7KBgVMhKcz9XfVkoyJI9ePC9q4W4v5pdethEIMH0vZ2m0EPRrs7a7Ix/cePWMfVCVHrFhhIu58qJohLhPKV7lw==');
define('LOGGED_IN_KEY',    'CykxU6zE1t0mIiyK0YQ/OdhxHe+6x2oTcbLHzeKP0vFO3wr7a34/m0yNK43yd4IRFpDJWVQNSnYjVu7QRfIRFw==');
define('NONCE_KEY',        '0X7Q7Tre7d5bjDDwYcHl+oo9OyUkafWx3NU2rUUEmEjhbp0LzvMEN0ebBUNK64LVIgg0q/x9X3cLoBMdlwjjGQ==');
define('AUTH_SALT',        'S9jhqFsolTXXNWOjPTP3vE89bhx5l5xg9bjDF5g3eD+fgdXgBbObPd3nIyaLcr8aUX2bCUZi/+FyLp0v0kVMPQ==');
define('SECURE_AUTH_SALT', 'KLi24lWbY3pK/n6y8xe+GFK0NvSbRjrZOxwbX3KY7u8JeJLx5lwe+eAmdS5H8oUFrk6IcbaeJNG02lOx+HV9fw==');
define('LOGGED_IN_SALT',   'yvDhxDtVDwTnlFXMjNU/rpOMYSlyIHBn7+uVHv1kzziJHaCRkqmAC5LeRF6Vjp9IqoWsGwjLUoq0Ob6A2i+nmQ==');
define('NONCE_SALT',       'm6pfxxrD91rD36kcbWgswJOH8serQbrID3sRxGUuMZR5uC4dLKIFT3b/njicwTVMXwOVoG5U6DUJw+GhXiSjlw==');

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';




/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';

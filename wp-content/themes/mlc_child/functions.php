<?php

/**
 * This is a very slim version of MLCworks theme builds 
 */


require get_theme_file_path() . '/lib/lib-helpers.php';
require get_theme_file_path() . '/lib/lib-acf-blocks.php';
require get_theme_file_path() . '/lib/lib-enqueue.php';

?>
module.exports = {
  plugins: [
    require("postcss-nested"),
    require("postcss-nested-import"),
    require("autoprefixer"),
    require("tailwindcss"),
  ],
};

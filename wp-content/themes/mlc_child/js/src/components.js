import { GallerySlider } from "./components/GallerySlider";
import { GalleryGrid } from "./components/GalleryGrid";
import { ExampleBlock } from "./components/ExampleBlock";

export default {
  GalleryGrid,
  GallerySlider,
  ExampleBlock,
};

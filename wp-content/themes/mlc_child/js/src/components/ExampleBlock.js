import React from "react";
import { htmlDecode } from "../helpers/functions";

// This block is here as a reference for working example of how Props is set up and passed
export function ExampleBlock({ title, content, image }) {
  return (
    <div className="my-8">
      {image && (
        <picture className="block mb-8">
          <img src={image.url} alt={image.alt} />
        </picture>
      )}
      {title && <h2 dangerouslySetInnerHTML={{ __html: htmlDecode(title) }} />}
      {content && (
        <div dangerouslySetInnerHTML={{ __html: htmlDecode(content) }} />
      )}
    </div>
  );
}

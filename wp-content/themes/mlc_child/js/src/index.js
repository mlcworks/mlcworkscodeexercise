/**
 * This file is an entry point that's loaded on every page.  For that reason, let's keep this
 * fairly minimal and avoid importing too many components here.
 */
import ReactHabitat from "react-habitat";
import React from "react";
import AppComponents from "./components";

import "./index.scss";

class App extends ReactHabitat.Bootstrapper {
  constructor() {
    super();

    const builder = new ReactHabitat.ContainerBuilder();

    Object.entries(AppComponents).forEach(([name, Component]) => {
      const WrappedComponent = (props) => <Component {...props} />;
      builder.register(WrappedComponent).as(name);
    });

    this.setContainer(builder.build());

    const parents = new Set();

    const observer = new MutationObserver((mutationList) => {
      mutationList.forEach((mutation) => {
        if (mutation.type !== "childList") {
          return;
        }

        if (!document.querySelector("[data-component]")) {
          observer.disconnect();
          document.body.classList.remove("loading");
        }
      });
    });
    Array.from(document.querySelectorAll("[data-component]")).forEach(
      (element) => {
        const parent = element.parentNode;
        parents.add(parent);
      }
    );

    parents.forEach((parent) => {
      observer.observe(parent, { childList: true });
    });
  }
}

new App();

if (module.hot) {
  module.hot.accept();
}

// React components do not render in Gutenberg Page Edit - Check if in Page Editing
if (typeof wp !== "undefined" && typeof wp.blocks !== "undefined") {
  // Hooks into ACF Append to Render App()
  acf.addAction("append", () => {
    new App();
  });
}

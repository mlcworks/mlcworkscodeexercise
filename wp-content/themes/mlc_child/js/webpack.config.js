const path = require("path");
const webpack = require("webpack");
const fs = require("fs");
const postcssNormalize = require("postcss-normalize");
const parentSelector = require("postcss-parent-selector");
const getCSSModuleLocalIdent = require("react-dev-utils/getCSSModuleLocalIdent");
const ReactRefreshWebpackPlugin = require("@pmmmwh/react-refresh-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const shouldUseSourceMap = process.env.GENERATE_SOURCEMAP !== "false";

const imageInlineSizeLimit = parseInt(
  process.env.IMAGE_INLINE_SIZE_LIMIT || "10000"
);

module.exports = () => {
  const isEnvProduction = process.env.NODE_ENV === "production";
  const isEnvDevelopment = process.env.NODE_ENV === "development";

  const getStyleLoaders = (cssOptions, preProcessor) => {
    const loaders = [
      isEnvDevelopment && require.resolve("style-loader"),
      isEnvProduction && {
        loader: MiniCssExtractPlugin.loader,
      },
      {
        loader: require.resolve("css-loader"),
        options: cssOptions,
      },
      {
        // Options for PostCSS as we reference these options twice
        // Adds vendor prefixing based on your specified browser support in
        // package.json
        loader: require.resolve("postcss-loader"),
        options: {
          // Necessary for external CSS imports to work
          ident: "postcss",
          plugins: () => [
            require("postcss-flexbugs-fixes"),
            require("postcss-preset-env")({
              autoprefixer: {
                flexbox: "no-2009",
              },
              stage: 3,
            }),
            require("tailwindcss"),
            require("autoprefixer"),
            // Adds PostCSS Normalizeas the reset css with default options,
            // so that it honors browserslist config in package.json
            // which in turn let's users customize the target behavior as per their needs.
            postcssNormalize(),
          ],
          sourceMap: isEnvProduction ? shouldUseSourceMap : isEnvDevelopment,
        },
      },
    ].filter(Boolean);
    if (preProcessor) {
      loaders.push(
        {
          loader: require.resolve("resolve-url-loader"),
          options: {
            sourceMap: isEnvProduction ? shouldUseSourceMap : isEnvDevelopment,
            root: path.join(__dirname, "src/"),
          },
        },
        {
          loader: require.resolve(preProcessor),
          options: {
            sourceMap: true,
          },
        }
      );
    }
    return loaders;
  };

  const getAdminStyleLoaders = (cssOptions, preProcessor) => {
    const loaders = [
      isEnvDevelopment && require.resolve("style-loader"),
      isEnvProduction && {
        loader: MiniCssExtractPlugin.loader,
      },
      {
        loader: require.resolve("css-loader"),
        options: cssOptions,
      },
      {
        // Options for PostCSS as we reference these options twice
        // Adds vendor prefixing based on your specified browser support in
        // package.json
        loader: require.resolve("postcss-loader"),
        options: {
          // Necessary for external CSS imports to work
          ident: "postcss",
          plugins: () => [
            require("postcss-flexbugs-fixes"),
            require("postcss-preset-env")({
              autoprefixer: {
                flexbox: "no-2009",
              },
              stage: 3,
            }),
            require("tailwindcss"),
            require("autoprefixer"),
            // Adds PostCSS Normalizeas the reset css with default options,
            // so that it honors browserslist config in package.json
            // which in turn let's users customize the target behavior as per their needs.
            postcssNormalize(),
            parentSelector({ selector: ".acf-block-preview" }),
          ],
          sourceMap: isEnvProduction ? shouldUseSourceMap : isEnvDevelopment,
        },
      },
    ].filter(Boolean);
    if (preProcessor) {
      loaders.push(
        {
          loader: require.resolve("resolve-url-loader"),
          options: {
            sourceMap: isEnvProduction ? shouldUseSourceMap : isEnvDevelopment,
            root: path.join(__dirname, "src/"),
          },
        },
        {
          loader: require.resolve(preProcessor),
          options: {
            sourceMap: true,
          },
        }
      );
    }
    return loaders;
  };

  function getContentBase(extraDirs = []) {
    if (!isEnvDevelopment) {
      return [];
    }

    const stat = fs.readdirSync(path.resolve(__dirname, "../"));

    return extraDirs.concat(
      stat
        .filter((fileOrFolder) => !fileOrFolder.endsWith("js"))
        .map((fileOrFolder) => path.resolve(__dirname, "../", fileOrFolder))
    );
  }

  return [
    {
      mode: isEnvDevelopment ? "development" : "production",
      bail: isEnvProduction,
      devtool: isEnvDevelopment ? "cheap-module-eval-source-map" : "source-map",
      output: {
        publicPath: isEnvDevelopment
          ? "http://localhost:8080/dist/"
          : "/wp-content/themes/mlc_child/js/dist/",
      },
      module: {
        strictExportPresence: true,
        rules: [
          {
            oneOf: [
              // "url" loader works like "file" loader except that it embeds assets
              // smaller than specified limit in bytes as data URLs to avoid requests.
              // A missing `test` is equivalent to a match.
              {
                test: [/\.bmp$/, /\.gif$/, /\.jpe?g$/, /\.png$/],
                loader: require.resolve("url-loader"),
                options: {
                  limit: imageInlineSizeLimit,
                  name: "static/media/[name].[hash:8].[ext]",
                },
              },
              {
                test: /\.(js|mjs|jsx|ts|tsx)$/,
                include: path.join(__dirname, "src/"),
                loader: require.resolve("babel-loader"),
                options: {
                  presets: [
                    [
                      "@babel/preset-env",
                      {
                        targets: "defaults",
                      },
                    ],
                    "@babel/preset-react",
                  ],

                  plugins: [
                    isEnvDevelopment && require.resolve("react-refresh/babel"),
                    [
                      require.resolve("babel-plugin-named-asset-import"),
                      {
                        loaderMap: {
                          svg: {
                            ReactComponent:
                              "@svgr/webpack?-svgo,+titleProp,+ref![path]",
                          },
                        },
                      },
                    ],
                  ].filter(Boolean),
                  // This is a feature of `babel-loader` for webpack (not Babel itself).
                  // It enables caching results in ./node_modules/.cache/babel-loader/
                  // directory for faster rebuilds.
                  cacheDirectory: true,
                  // See #6846 for context on why cacheCompression is disabled
                  cacheCompression: false,
                  compact: isEnvProduction,
                },
              },
              {
                test: /\.scss$/,
                exclude: [/\.module\.scss$/],
                use: getStyleLoaders(
                  {
                    importLoaders: 3,
                    sourceMap: isEnvProduction
                      ? shouldUseSourceMap
                      : isEnvDevelopment,
                  },
                  "sass-loader"
                ),
                sideEffects: true,
              },
              {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
              },
              {
                test: /\.module\.scss$/,
                use: getStyleLoaders(
                  {
                    importLoaders: 3,
                    sourceMap: shouldUseSourceMap,
                    modules: {
                      getLocalIdent: getCSSModuleLocalIdent,
                    },
                  },
                  "sass-loader"
                ),
              },
              {
                loader: require.resolve("file-loader"),
                exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
                options: {
                  name: "static/media/[name].[ext]",
                },
              },
              // ** STOP ** Are you adding a new loader?
              // Make sure to add the new loader(s) before the "file" loader.
            ],
          },
        ],
      },
      resolve: {
        alias: {
          react: path.resolve("./node_modules/react"),
          "react-dom": path.resolve("./node_modules/react-dom"),
        },
      },
      devServer: {
        host: "0.0.0.0",
        contentBase: getContentBase(),
        watchContentBase: true,
        disableHostCheck: true,
        hot: true,
        stats: "minimal",
        headers: {
          "Access-Control-Allow-Origin": "*",
          "Access-Control-Allow-Methods":
            "GET, POST, PUT, DELETE, PATCH, OPTIONS",
          "Access-Control-Allow-Headers":
            "X-Requested-With, content-type, Authorization",
        },
      },
      plugins: [
        new webpack.DefinePlugin({
          "process.env": {
            NODE_ENV: JSON.stringify(
              process.env.NODE_ENV === "development"
                ? "development"
                : "production"
            ),
          },
        }),
        isEnvDevelopment && new webpack.HotModuleReplacementPlugin(),
        isEnvDevelopment && new ReactRefreshWebpackPlugin(),
        isEnvProduction &&
          new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            // TODO: fix paths
            filename: "static/css/[name].css",
            chunkFilename: "static/css/[name].chunk.css",
          }),
      ].filter(Boolean),
    },
    {
      mode: isEnvDevelopment ? "development" : "production",
      output: {
        publicPath: "/wp-content/themes/mlc_child/dist/",
      },
      module: {
        rules: [
          {
            oneOf: [
              {
                test: /\.(js|mjs|jsx|ts|tsx)$/,
                include: path.join(__dirname, "src/"),
                loader: isEnvDevelopment
                  ? "ignore-loader"
                  : require.resolve("babel-loader"),
                options: {
                  presets: [
                    [
                      "@babel/preset-env",
                      {
                        targets: "defaults",
                      },
                    ],
                    "@babel/preset-react",
                  ],

                  plugins: [
                    isEnvDevelopment && require.resolve("react-refresh/babel"),
                    [
                      require.resolve("babel-plugin-named-asset-import"),
                      {
                        loaderMap: {
                          svg: {
                            ReactComponent:
                              "@svgr/webpack?-svgo,+titleProp,+ref![path]",
                          },
                        },
                      },
                    ],
                  ].filter(Boolean),
                  // This is a feature of `babel-loader` for webpack (not Babel itself).
                  // It enables caching results in ./node_modules/.cache/babel-loader/
                  // directory for faster rebuilds.
                  cacheDirectory: true,
                  // See #6846 for context on why cacheCompression is disabled
                  cacheCompression: false,
                  compact: isEnvProduction,
                },
              },
              {
                test: /\.scss$/,
                exclude: [/\.module\.scss$/],
                use: getAdminStyleLoaders(
                  {
                    importLoaders: 3,
                    sourceMap: isEnvProduction
                      ? shouldUseSourceMap
                      : isEnvDevelopment,
                  },
                  "sass-loader"
                ),
                sideEffects: true,
              },
              {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
              },
              {
                test: /\.module\.scss$/,
                use: getAdminStyleLoaders(
                  {
                    importLoaders: 3,
                    sourceMap: shouldUseSourceMap,
                    modules: {
                      getLocalIdent: getCSSModuleLocalIdent,
                    },
                  },
                  "sass-loader"
                ),
              },
              {
                loader: isEnvDevelopment
                  ? "ignore-loader"
                  : require.resolve("file-loader"),
                exclude: [/\.(js|mjs|jsx|ts|tsx)$/, /\.html$/, /\.json$/],
                options: {
                  name: "static/media/[name].[ext]",
                },
              },
              // ** STOP ** Are you adding a new loader?
              // Make sure to add the new loader(s) before the "file" loader.
            ],
          },
        ],
      },
      plugins: [
        isEnvProduction &&
          new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            // TODO: fix paths
            filename: "static/css/admin/[name].css",
            chunkFilename: "static/css/admin/[name].chunk.css",
          }),
      ].filter(Boolean),
    },
  ];
};

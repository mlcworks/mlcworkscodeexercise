<?php 

defined( 'ABSPATH' ) or die();

if( function_exists('acf_register_block') ) {
	add_action('acf/init', 'acf_mlc_blocks');
}

function acf_mlc_blocks() {
	$keys = ['name', 'title', 'description', 'render_callback', 'category', 'icon', 'keywords'];
	$blocks = [
		[	
			'name'				=> 'example-block',
			'title'				=> __('Example Block'),
			'description'		=> __(''),
			'render_callback'	=> 'acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'example', 'block' , 'example block'),
		],
		[	
			'name'				=> 'gallery-slider',
			'title'				=> __('Gallery Slider'),
			'description'		=> __(''),
			'render_callback'	=> 'acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'gallery', 'slider' , 'gallery slider'),
		],
		[	
			'name'				=> 'gallery-grid',
			'title'				=> __('Gallery Grid'),
			'description'		=> __(''),
			'render_callback'	=> 'acf_block_render_callback',
			'category'			=> 'formatting',
			'icon'				=> 'admin-comments',
			'keywords'			=> array( 'gallery', 'grid' , 'gallery grid'),
		],
	];

	foreach ($blocks as $block) {
		foreach ($keys as $key) {
			assert(array_key_exists($key, $block));
		}

		acf_register_block($block);
	}
}

function acf_block_render_callback( $block ) {
	$slug = str_replace('acf/', '', $block['name']);
	
	// include a template part from within the "template-parts/block" folder
	if( file_exists( get_theme_file_path("/template-parts/acf/{$slug}.php") ) ) {
		include( get_theme_file_path("/template-parts/acf/{$slug}.php") );
	}
}
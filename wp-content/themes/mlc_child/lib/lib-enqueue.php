<?php 

/**
 * Enqueue all Theme Scripts & Styles
 *
 * @package MLCworks
 * @subpackage sprout_pediatrics
 */

defined( 'ABSPATH' ) or die();

add_action( 'wp_enqueue_scripts', 'theme_styles');

add_action( 'wp_enqueue_scripts', 'theme_scripts' );

/**
 * Register and/or Enqueue
 * Styles for the theme
 */
function theme_styles() {

	$theme_dir = get_stylesheet_directory_uri();

	if ($_ENV['NODE_ENV'] === 'production') {
		wp_enqueue_style( 'main', "$theme_dir/js/dist/static/css/main.css", array(), '1.1.3' );
	}
}

/**
 * Register and/or Enqueue
 * Scripts for the theme
 *
 */
function theme_scripts() {
	$theme_dir = get_stylesheet_directory_uri();
	if ($_ENV['NODE_ENV'] === 'development') {
		wp_enqueue_script( 'main', "{$_ENV['WEBPACK_DEV_SERVER_HOST']}/dist/main.js", array(), false, true);
	} else {
		wp_enqueue_script( 'main', "{$theme_dir}/js/dist/main.js", array(), '1.2.8', true );
	}
}
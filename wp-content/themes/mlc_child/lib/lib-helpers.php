<?php

    /** 
	 * Converts strings special characters & entities, used to pass content to React
	 * @param  string
	 * @return string
	 */
	function mlc_entities($string) {
		return htmlspecialchars(htmlentities($string), ENT_QUOTES);
	}

?>
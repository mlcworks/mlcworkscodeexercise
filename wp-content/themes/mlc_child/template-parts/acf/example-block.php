<div data-component="ExampleBlock" data-props='<?= json_encode(array(
	'title' => mlc_entities(get_field('title')),
	'content' => mlc_entities(get_field('content')),
	'image' => get_field('image'),
)); ?>'></div>
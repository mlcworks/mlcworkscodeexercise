<?php
/**
 * Gallery Slider
 *
 * Pass ACF Field data inside the data-props attribute below
 * 
 * Note: the function mlc_entities() as you see below that invokes around get_field is a helper function (mlc_docs/lib/lib-helpers.php) set
 * up to convert a string to its HTML Entities. By doing this, this will prevent special characters (such as apostrophes) from breaking our JSON string
 * that is used to pass data to the React Components props.
 * 
 */

?>

<div data-component="GallerySlider" data-props='<?= json_encode(array(
	// 'example' => get_field('example'),
	// 'text_example' => mlc_entities(get_field('text_example')),
)); ?>'></div>